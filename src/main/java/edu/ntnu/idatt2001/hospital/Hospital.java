package edu.ntnu.idatt2001.hospital;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;

public class Hospital {
    final String hospitalName;
    ArrayList<Department> allDepartments = new ArrayList<>();

    /**
     * The constructor for the Hospital.
     */
    public Hospital(String hospitalName) {
        this.hospitalName = hospitalName;
    }

    /**
     * Getters and setters for hospital class.
     */
    public String getHospitalName() {
        return hospitalName;
    }
    public ArrayList<Department> getDepartments (){
        return allDepartments;
    }
    public void addDepartment(Department department){
        allDepartments.add(department);
    }

    public String toString(){
        return "Hospital name: " + hospitalName;
    }
}

class Department {
    String departmentName;
    ArrayList<Employee> allEmployees = new ArrayList<>();
    ArrayList<Patient> allPatients = new ArrayList<>();

    /**
     * The constructor for the Department.
     */
    public Department(String departmentName) {
        this.departmentName = departmentName;
    }

    /**
     * Getters and setters for department class.
     */
    public String getDepartmentName() {
        return departmentName;
    }
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }
    public ArrayList<Employee> getEmployees(){
        return allEmployees;
    }
    public void addEmployee(Employee e){
        allEmployees.add(e);
    }
    public ArrayList<Patient> getPatients(){
        return allPatients;
    }
    public void addPatient(Patient p){
        allPatients.add(p);
    }

    /**
     * Remove a person from both Employee and Patient registers by iterating through every person.
     * I could also use instanceOf, but chose to use my equals method instead.
     *
     * @param person person that will be removed.
     * @throws RemoveException if the person does not exist in any registers.
     */
    public void remove (Person person) throws RemoveException {
        boolean removed = false;
        Iterator<Patient> patientIterator = allPatients.iterator();
        while (patientIterator.hasNext()) {
            Patient patient = patientIterator.next();
            if (patient.equals(person)) {
                patientIterator.remove();
                removed = true;
            }
        }
        Iterator<Employee> employeeIterator = allEmployees.iterator();
        while (employeeIterator.hasNext()){
            Employee employee = employeeIterator.next();
            if (employee.equals(person)){
                employeeIterator.remove();
                removed = true;
            }
        }
        if (!removed) {
            throw new RemoveException(person.getFullname());
        }
    }

    /**
     * Hashcode and equals method used the department class.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Department)) return false;
        Department that = (Department) o;
        return Objects.equals(departmentName, that.departmentName) && Objects.equals(allEmployees, that.allEmployees) && Objects.equals(allPatients, that.allPatients);
    }

    @Override
    public int hashCode() {
        return Objects.hash(departmentName, allEmployees, allPatients);
    }

    public String toString(){
        return "Department name: " + departmentName;
    }
}

/**
 * Exception that will be thrown if the person does not exist in any registers.
 */
class RemoveException extends Exception{

    public RemoveException(String message) {
        super("Person does not exist: " + message);
    }
}