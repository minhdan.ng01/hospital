package edu.ntnu.idatt2001.hospital;

public abstract class Person {
    private String firstname;
    private String surname;
    private String socialSecurityNumber;

    /**
     * The constructor for the Person object.
     *
     * @param firstname Person's firstname
     * @param surname Persons's lastname
     * @param socialSecurityNumber Person's Social security number
     */

    public Person(String firstname, String surname, String socialSecurityNumber) {
        this.firstname = firstname;
        this.surname = surname;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    /**
     * Access methods
     */
    public String getFirstname() {
        return firstname;
    }

    public String getSurname() {
        return surname;
    }

    public String getFullname() {
        return firstname + " " + surname;
    }
    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    /**
     * Set methods
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String toString(){
        return "Person name: " + getFullname();
    }
}

class Employee extends Person{
    /**
     * The constructor for an Employee.
     */
    public Employee(String firstname, String surname, String socialSecurityNumber) {
        super(firstname, surname, socialSecurityNumber);
    }
    public String toString(){
        return "Employee name: " + getFullname();
    }
}

class Nurse extends Employee{
    /**
     * The constructor for a nurse
     */
    public Nurse(String firstname, String surname, String socialSecurityNumber) {
        super(firstname, surname, socialSecurityNumber);
    }

    public String toString(){
        return "Nurse name: " + getFullname();
    }
}

abstract class Doctor extends Employee{
    /**
     * The constructor for a doctor.
     */
    public Doctor(String firstname, String surname, String socialSecurityNumber) {
        super(firstname, surname, socialSecurityNumber);
    }
    public abstract void setDiagnosis(Patient patient, String diagnosis);
}

class Surgeon extends Doctor{
    /**
     * The constructor for a surgeon.
     */
    public Surgeon(String firstname, String surname, String socialSecurityNumber) {
        super(firstname, surname, socialSecurityNumber);
    }

    public void setDiagnosis(Patient patient, String diagnosis) {

    }
}
class GeneralPractitioner extends Doctor{
    /**
     * The constructor for a general practitioner.
     */
    public GeneralPractitioner(String firstname, String surname, String socialSecurityNumber) {
        super(firstname, surname, socialSecurityNumber);
    }

    public void setDiagnosis(Patient patient, String diagnosis) {

    }

}

class Patient extends Person implements Diagnosable{
    private String diagnosis = "";

    /**
     * The constructor for a patient.
     */
    public Patient(String firstname, String surname, String socialSecurityNumber) {
        super(firstname, surname, socialSecurityNumber);
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String toString(){
        return "Patient name: " + getFullname();
    }
}

