package edu.ntnu.idatt2001.hospital;

interface Diagnosable{
    public void setDiagnosis(String diagnosis);
}
