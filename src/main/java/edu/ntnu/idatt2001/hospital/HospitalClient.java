package edu.ntnu.idatt2001.hospital;

public class HospitalClient {
    public static void main(String[] args) throws RemoveException {
        // Creating a hospital object.
        Hospital hospital = new Hospital("Medic");
        // Fill the hospital with data.
        HospitalTestData.fillRegisterWithTestData(hospital);

        // Creating a department object using the hospital's children polyclinic.
        Department childrenPolyclinic = hospital.getDepartments().get(1);

        System.out.println("Before: " + childrenPolyclinic.getEmployees().toString());
        // Removing the employee Salti Kaffen
        childrenPolyclinic.remove(childrenPolyclinic.getEmployees().get(0));
        // Updated list with employees
        System.out.println("After: " + childrenPolyclinic.getEmployees().toString());

        // Create a patient and attempt to remove them from a list
        Patient patient = new Patient("Olga", "Mutlebust", "123");
        try{
            childrenPolyclinic.remove(patient);
        }
        catch (RemoveException e){
            throw new RemoveException(patient.getFullname());
        }
    }
}

final class HospitalTestData {
    private HospitalTestData() {
        // not called
    }

    /**
     * @param hospital the hospital.
     */
    public static void fillRegisterWithTestData(final Hospital hospital) {
        // Add some departments
        Department emergency = new Department("Akutten");
        emergency.getEmployees().add(new Employee("Odd Even", "Primtallet", ""));
        emergency.getEmployees().add(new Employee("Huppasahn", "DelFinito", ""));
        emergency.getEmployees().add(new Employee("Rigmor", "Mortis", ""));
        emergency.getEmployees().add(new GeneralPractitioner("Inco", "Gnito", ""));
        emergency.getEmployees().add(new Surgeon("Inco", "Gnito", ""));
        emergency.getEmployees().add(new Nurse("Nina", "Teknologi", ""));
        emergency.getEmployees().add(new Nurse("Ove", "Ralt", ""));
        emergency.getPatients().add(new Patient("Inga", "Lykke", ""));
        emergency.getPatients().add(new Patient("Ulrik", "Smål", ""));
        hospital.getDepartments().add(emergency);
        Department childrenPolyclinic = new Department("Barn poliklinikk");
        childrenPolyclinic.getEmployees().add(new Employee("Salti", "Kaffen", ""));
        childrenPolyclinic.getEmployees().add(new Employee("Nidel V.", "Elvefølger", ""));
        childrenPolyclinic.getEmployees().add(new Employee("Anton", "Nym", ""));
        childrenPolyclinic.getEmployees().add(new GeneralPractitioner("Gene", "Sis", ""));
        childrenPolyclinic.getEmployees().add(new Surgeon("Nanna", "Na", ""));
        childrenPolyclinic.getEmployees().add(new Nurse("Nora", "Toriet", ""));
        childrenPolyclinic.getPatients().add(new Patient("Hans", "Omvar", ""));
        childrenPolyclinic.getPatients().add(new Patient("Laila", "La", ""));
        childrenPolyclinic.getPatients().add(new Patient("Jøran", "Drebli", ""));
        hospital.getDepartments().add(childrenPolyclinic);
    }
}
