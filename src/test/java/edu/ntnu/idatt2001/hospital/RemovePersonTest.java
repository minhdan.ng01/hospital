package edu.ntnu.idatt2001.hospital;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
class RemovePersonTest {

    @Nested
    class RemovePersonTests {
        @Test
        void removeAnExistingPatient() throws RemoveException {
            Patient testPerson = new Patient("Test", "Tests", "12345");
            Department testDept = new Department("Test Department");
            testDept.addPatient(testPerson);
            testDept.remove(testPerson);

            assertTrue(testDept.getPatients().isEmpty());
        }
        @Test
        void removeAnExistingEmployee() throws RemoveException {
            Surgeon testPerson = new Surgeon("Test", "Tests", "12345");
            Department testDept = new Department("Test Department");
            testDept.addEmployee(testPerson);
            testDept.remove(testPerson);

            assertTrue(testDept.getEmployees().isEmpty());
        }

        @Test
        void removeANonExistentPatient() {
            Patient testPerson = new Patient("Test", "Tests", "12345");
            Department testDept = new Department("Test Department");

            Assertions.assertThrows(RemoveException.class, () -> testDept.remove(testPerson));

        }
    }
}